from PIL import Image
import argparse
import numpy as np
import crccalculator as cc 
import sobelref as sr

def save_image_header(image, header_file_name):
    """
    Generate C/C++ header file with image source.
    image - PIL image object.
    filename - string, including ".h"
    """
    sobel, _, _ = sr.calculate_sobel(np.array(image, dtype=np.uint8))
    crc = cc.calculate_crc32(sobel[1:-1, 1:-1])

    header_src = generate_image_header(image.im, image.width, image.height, crc, header_file_name[:-2])
    with open(header_file_name, "w") as header_file:
        header_file.write(header_src)

def generate_image_header(image_arr, width, height, result_crc=None, name=None):
    """
    Generate C/C++ header file content (as a string)
    with image source.
    image_arr - 1d array of grayscale pixels
    """
    header_text = "#ifndef __IMAGE_SRC_HEADER\n#define __IMAGE_SRC_HEADER\n"
    
    header_text += "\n"
    
    header_text += "#define WIDTH {}\n".format(width)
    header_text += "#define HEIGHT {}\n".format(height)

    header_text += "#define IMAGE_SRC {"
    for i in image_arr:
        header_text += str(i) + ", " 
    header_text = header_text[:-2] + "}\n"

    if name != None:
        header_text += "#define IMAGE_NAME \"{}\"\n".format(name) 

    if result_crc != None:
        header_text += "#define RESULT_CRC32 {}\n".format(hex(result_crc))

    header_text += "\n"
    header_text += "#endif // __IMAGE_SRC_HEADER"

    return header_text

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert images to C/C++ header files")
    parser.add_argument("image_file_name", type=str, help="Input image file")
    args = parser.parse_args()

    image = Image.open(args.image_file_name).convert("L")
    header_file_name = args.image_file_name.split(".")[0] + ".h"

    save_image_header(image, header_file_name)

"""
This test checks whether the reference python Sobel operator implementation
is equal to the OpenCV Sobel operator implementation.
"""

import numpy as np
import crccalculator as cc 
import sobelref as sr
import unittest
import cv2

def calculate_sobel_opencv(image):
    """
    OpenCV sobel implementation.
    """
    g_x = cv2.Sobel(image, cv2.CV_16S, 1, 0, ksize=3, borderType=cv2.BORDER_ISOLATED)
    g_y = cv2.Sobel(image, cv2.CV_16S, 0, 1, ksize=3, borderType=cv2.BORDER_ISOLATED)
    sobel = np.absolute(g_x) // 6 + np.absolute(g_y) // 6 
    sobel = np.uint8(sobel)
    return sobel, g_x, g_y

class TestSobelRef(unittest.TestCase):
    def test_sobel_ref(self):
        # Genarate random image and cast it to uint8
        image = np.random.rand(100, 100) * 255
        image = np.array(image, dtype=np.uint8)

        # Apply Sobel operator
        sobel_ocv, g_ocv_x, g_ocv_y = calculate_sobel_opencv(image)
        sobel, g_x, g_y = sr.calculate_sobel(image) 

        # Compare results via CRC comparison
        crc_g_x = cc.calculate_crc32(g_x[1:-1,1:-1])
        crc_g_ocv_x = cc.calculate_crc32(g_ocv_x[1:-1,1:-1])
        self.assertEqual(crc_g_x, crc_g_ocv_x)

        crc_g_y = cc.calculate_crc32(g_y[1:-1,1:-1])
        crc_g_ocv_y = cc.calculate_crc32(g_ocv_y[1:-1,1:-1])
        self.assertEqual(crc_g_y, crc_g_ocv_y)

        crc_sobel = cc.calculate_crc32(sobel[1:-1,1:-1])
        crc_sobel_ocv = cc.calculate_crc32(sobel_ocv[1:-1,1:-1])
        self.assertEqual(crc_sobel, crc_sobel_ocv)

if __name__ == '__main__':
    unittest.main()
    
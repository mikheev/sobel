import zlib
import numpy as np
import unittest

def calculate_crc32(buffer):
    """
    Cast bufer to np.uint8 and calculate CRC32.
    """
    buffer = np.array(buffer, dtype=np.uint8)
    return zlib.crc32(buffer.tobytes())


class TestCRC(unittest.TestCase):
    def test_crc32(self):
        test_arr = [1, 2, 3, 4, 5, 6, 7, 8]
        expected_crc = 0x3fca88c5

        crc = calculate_crc32(test_arr)

        self.assertEqual(crc, expected_crc)

if __name__ == '__main__':
    unittest.main()

"""
Reference implementaion of Sobel filter.
"""
import numpy as np

def calculate_sobel(image):
    """
    Apply sobel operator to the image
    G_x = C(A, k_x)
    G_y = C(A, k_y)
    G = |G_x| / 8 + |G_y| / 8
    
    1 / 8 - to fit result to np.uint8
    
    Input image should be a 2D numpy array.
    Output image has the same dimensions. 
    Boundary points will be filled with zeros.
    """
    # Gradient kernels
    k_x = np.array([[-1, 0, 1],
                    [-2, 0, 2],
                    [-1, 0, 1]], dtype=np.int8)

    k_y = np.array([[-1, -2, -1],
                    [ 0,  0,  0],
                    [ 1,  2,  1]], dtype=np.int8)

    # Arrays for gradients
    g_x = np.zeros((image.shape[0], image.shape[1]))
    g_y = np.zeros((image.shape[0], image.shape[1]))

    # Process points
    for y in range(1, image.shape[0] - 1):
        for x in range(1, image.shape[1] - 1):
            # Calculate correlation functions for each point
            for i in [-1, 0, 1]:
                for j in [-1, 0, 1]:
                    g_x[y, x] += image[y + i, x + j] * k_x[i + 1, j + 1]
                    g_y[y, x] += image[y + i, x + j] * k_y[i + 1, j + 1]

    # Calculate gradient l1 norm
    g = np.abs(g_x) // 6 + np.abs(g_y) // 6
    g = np.array(g, dtype=np.uint8)
    return g, g_x, g_y
    
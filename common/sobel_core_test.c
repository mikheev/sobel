#include "acutest.h"
#include "sobel_core.h"
#include "sobel_core.h"

void test_im_idx(void)
{
    //           Cols: 0  1  2  3  4   // Lines
    uint8_t image[] = {0, 0, 0, 0, 0,  // 0
                       0, 0, 1, 0, 0,  // 1
                       0, 0, 0, 0, 0}; // 2

    uint32_t width = 5;
    uint32_t x = 2;
    uint32_t y = 1;
    uint8_t expected_pix_value = 1;

    uint32_t idx = im_idx(width, x, y);

    TEST_CHECK(image[idx] == expected_pix_value);
}

void test_apply_sobel_to_point(void)
{
    uint8_t image[] = {1, 2, 3, 
                       4, 5, 6, 
                       7, 8, 9};
    uint8_t expected_result = 5;
    uint8_t result = apply_sobel_to_point(image, 3, 1, 1);

    TEST_CHECK(result == expected_result);

}  

TEST_LIST = {
    {"getting image buf indexes", test_im_idx},
    {"sobel operator for single point", test_apply_sobel_to_point},
    {NULL, NULL }
};
#ifndef __SOBEL_CORE_HEADER
#define __SOBEL_CORE_HEADER

#include "stdint.h"
#include "stdlib.h"

// Kernel size
#define K_SIZE_X    3
#define K_SIZE_Y    3

/*  -- X kernel --
 *  | -1, 0, 1 |
 *  | -2, 0, 2 |
 *  | -1, 0, 1 |
 */
#define K_X_00  -1
#define K_X_01   0
#define K_X_02   1
#define K_X_10  -2
#define K_X_11   0
#define K_X_12   2
#define K_X_20  -1
#define K_X_21   0
#define K_X_22   1

/*  -- Y kernel --
 * |-1, -2, -1 |
 * | 0,  0,  0 |
 * | 1,  2,  1 |
 */
#define K_Y_00  -1
#define K_Y_01  -2
#define K_Y_02  -1
#define K_Y_10   0
#define K_Y_11   0
#define K_Y_12   0
#define K_Y_20   1
#define K_Y_21   2
#define K_Y_22   1

/*
 * Calculate sobel gradient for a single point.
 * G_x = C(A, k_x)
 * G_y = C(A, k_y)
 * G = |G_x| / 6 + |G_y| / 6
 * 
 * 1 / 6 - to fit result to uint8_t
 * 
 * #OptimizeIt!
 * Avoid index calculation in style like (y + 1) * width + x - 1 by using precalculated offsets.
 */ 
#ifdef __CUDA_ARCH__
__device__
#endif
static inline uint8_t apply_sobel_to_point(uint8_t *image, uint32_t width, uint32_t x, uint32_t y)
{
    return abs(
        image[(y - 1) * width + x - 1] * K_X_00 +
        //image[(y - 1) * width + x    ] * K_X_01 +
        image[(y - 1) * width + x + 1] * K_X_02 +
        image[(y    ) * width + x - 1] * K_X_10 +
        //image[(y    ) * width + x    ] * K_X_11 +
        image[(y    ) * width + x + 1] * K_X_12 +
        image[(y + 1) * width + x - 1] * K_X_20 +
        //image[(y + 1) * width + x    ] * K_X_21 +
        image[(y + 1) * width + x + 1] * K_X_22
        ) / 6 + abs(
        image[(y - 1) * width + x - 1] * K_Y_00 +
        image[(y - 1) * width + x    ] * K_Y_01 +
        image[(y - 1) * width + x + 1] * K_Y_02 +
        //image[(y    ) * width + x - 1] * K_Y_10 +
        //image[(y    ) * width + x    ] * K_Y_11 +
        //image[(y    ) * width + x + 1] * K_Y_12 +
        image[(y + 1) * width + x - 1] * K_Y_20 +
        image[(y + 1) * width + x    ] * K_Y_21 +
        image[(y + 1) * width + x + 1] * K_Y_22
        ) / 6;
}

// Replace "/ 6" by "* 0x5556 >> 17"
#ifdef __CUDA_ARCH__
__device__
#endif
static inline uint8_t apply_sobel_to_point_optimized_div(uint8_t *image, uint32_t width, uint32_t x, uint32_t y)
{
    return (abs(
        image[(y - 1) * width + x - 1] * K_X_00 +
        //image[(y - 1) * width + x    ] * K_X_01 +
        image[(y - 1) * width + x + 1] * K_X_02 +
        image[(y    ) * width + x - 1] * K_X_10 +
        //image[(y    ) * width + x    ] * K_X_11 +
        image[(y    ) * width + x + 1] * K_X_12 +
        image[(y + 1) * width + x - 1] * K_X_20 +
        //image[(y + 1) * width + x    ] * K_X_21 +
        image[(y + 1) * width + x + 1] * K_X_22
        ) * 0x5556 >> 17) + (abs(
        image[(y - 1) * width + x - 1] * K_Y_00 +
        image[(y - 1) * width + x    ] * K_Y_01 +
        image[(y - 1) * width + x + 1] * K_Y_02 +
        //image[(y    ) * width + x - 1] * K_Y_10 +
        //image[(y    ) * width + x    ] * K_Y_11 +
        //image[(y    ) * width + x + 1] * K_Y_12 +
        image[(y + 1) * width + x - 1] * K_Y_20 +
        image[(y + 1) * width + x    ] * K_Y_21 +
        image[(y + 1) * width + x + 1] * K_Y_22
        ) * 0x5556 >> 17);
}


/*
 * Calculate sobel gradient for a points packed by 4 in 32-bit word.
 * G_x = C(A, k_x)
 * G_y = C(A, k_y)
 * G = |G_x| / 6 + |G_y| / 6
 * 
 * 1 / 6 - to fit result to uint8_t
 * 
 * #OptimizeIt!
 * Avoid index calculation in style like (y + 1) * width + x - 1 by using precalculated offsets.
 */ 
#ifdef __CUDA_ARCH__
__device__
#endif
static inline uint32_t apply_sobel_to_point_32(uint32_t *image, uint32_t width_words, uint32_t x, uint32_t y)
{
    uint32_t result = 0;
    
    // 0x000000vv
    // x - 1: [x    ] & 0x0000ff00 >> 8
    // x    : [x    ] & 0x000000ff >> 0
    // x + 1: [x + 1] & 0xff000000 >> 24
    result |= abs(
        ((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_00 +
      //((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_01 +
        ((image[(y - 1) * width_words + x + 1] & 0xff000000) >> 24) * K_X_02 +
        ((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_10 +
      //((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_X_11 +
        ((image[(y    ) * width_words + x + 1] & 0xff000000) >> 24) * K_X_12 +
        ((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8)* K_X_20 +
      //((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_21 +
        ((image[(y + 1) * width_words + x + 1] & 0xff000000) >> 24) * K_X_22
        ) / 6 + abs(
        ((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_00 +
        ((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_01 +
        ((image[(y - 1) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_02 +
      //((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_10 +
      //((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_11 +
      //((image[(y    ) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_12 +
        ((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_20 +
        ((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_21 +
        ((image[(y + 1) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_22
        ) / 6;


    // 0x0000vv00
    // x - 1: [x    ] & 0x00ff0000 >> 16
    // x    : [x    ] & 0x0000ff00 >> 8
    // x + 1: [x    ] & 0x000000ff << 0
    result |= (abs(
        ((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_00 +
      //((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_01 +
        ((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_02 +
        ((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_10 +
      //((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_11 +
        ((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_X_12 +
        ((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16)* K_X_20 +
      //((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_21 +
        ((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_22
        ) / 6 + abs(
        ((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_00 +
        ((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_01 +
        ((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_02 +
      //((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_10 +
      //((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_11 +
      //((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_12 +
        ((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_20 +
        ((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_21 +
        ((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_22
        ) / 6) << 8;
     

    // 0xvv000000
    // x - 1: [x    ] & 0xff000000 >> 24
    // x    : [x    ] & 0x00ff0000 >> 16
    // x + 1: [x    ] & 0x0000ff00 >> 8
    result |= (abs(
        ((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_00 +
      //((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_01 +
        ((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_02 +
        ((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_X_10 +
      //((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_11 +
        ((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_12 +
        ((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24)* K_X_20 +
      //((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_21 +
        ((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_22
        ) / 6 + abs(
        ((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_00 +
        ((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_01 +
        ((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_02 +
      //((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_Y_10 +
      //((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_11 +
      //((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_12 +
        ((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_20 +
        ((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_21 +
        ((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_22
        ) / 6) << 16;
        
      
    // 0x000000vv
    // x - 1: [x - 1] & 0x000000ff >> 0
    // x    : [x    ] & 0xff000000 >> 24
    // x + 1: [x    ] & 0x00ff0000 >> 16
    result |= (abs(
        ((image[(y - 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_X_00 +
      //((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_01 +
        ((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_02 +
        ((image[(y    ) * width_words + x - 1] & 0x000000ff) >> 0) * K_X_10 +
      //((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_X_11 +
        ((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_12 +
        ((image[(y + 1) * width_words + x - 1] & 0x000000ff) >> 0)* K_X_20 +
      //((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_21 +
        ((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_22
        ) / 6 + abs(
        ((image[(y - 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_00 +
        ((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_01 +
        ((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_02 +
      //((image[(y    ) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_10 +
      //((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_Y_11 +
      //((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_12 +
        ((image[(y + 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_20 +
        ((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_21 +
        ((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_22
        ) / 6) << 24;
    return result;
}

/*
 * Calculate index for point with coordinates (x, y) of 2D image
 * in 1D image buffer. 
 */ 
#ifdef __CUDA_ARCH__
__device__
#endif
static inline uint32_t im_idx(uint32_t width, uint32_t x, uint32_t y)
{
    return y * width + x;
}

#endif // __SOBEL_CORE_HEADER
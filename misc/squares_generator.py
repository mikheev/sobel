import numpy as np
from PIL import Image

square_size = 16
squares_x = 8
squares_y = 2

image = np.zeros((square_size * squares_y + 2, square_size * squares_x + 2))

colors = [0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff]

for i in range(squares_x):
    for j in range(squares_y):
        image[1 + j * square_size:1 + (j+1) * square_size, 
              1 + i * square_size:1 + (i+1) * square_size] = colors[i + squares_y * j]

image_pil = Image.fromarray(image.astype(np.uint8))
image_pil.save("squares.png")
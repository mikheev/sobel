#include "malt_ext.h"
#include "stdio.h"
#include "stdbool.h"
#include "string.h"

#include "../../common/sobel_core.h"
#include "../../common/crc_check.h"
#include "../../common/lwlog.h"

//#include "../../images/rand_66.h"
#include "../../images/tram_1026.h"

#define PROC_REGION_SIZE_X 16
#define PROC_REGION_SIZE_Y 16

#define CHUNK_SIZE      ((PROC_REGION_SIZE_X + 2) * (PROC_REGION_SIZE_Y + 2))   // Words per processing element

#define MEM_ALIGNMENT 16

#define MOVE_GLOBAL_BUFFERS_TO_HEAP
//#define ALIGNED_MEMORY_ACCESS

// Calculation settings
#define READ_GLOBAL_MEM_ENABLE
#define PROCESS_DATA_ENABLE
#define WRITE_GLOBAL_MEM_ENABLE
#define CHECK_CRC

uint8_t input_image_temp[] __attribute__ ((aligned (MEM_ALIGNMENT))) = IMAGE_SRC;
uint8_t output_image_temp[(WIDTH - 2) * (HEIGHT - 2)] __attribute__ ((aligned (MEM_ALIGNMENT))) = {1};


////////  Slave functions  ///////////

int slave_proces_image(uint8_t * const input_image, uint8_t * const output_image, int slave_id, int slaves_total)
{
    // Get slave id
    const int num_regions_x = (WIDTH - 2) / PROC_REGION_SIZE_X;
    const int num_regions_y = (HEIGHT - 2) / PROC_REGION_SIZE_Y;
    const int num_regions = num_regions_x * num_regions_y;
        
    
    // Create the two input vectors
    volatile uint8_t input_buffer[CHUNK_SIZE];
    volatile uint8_t output_buffer[CHUNK_SIZE];
    
    int i, j, k;
    int region_idx, region_idx_x, region_idx_y;
    
    for (region_idx = slave_id; region_idx < num_regions; region_idx += slaves_total)
    {
        region_idx_x = region_idx % num_regions_x;
        region_idx_y = region_idx / num_regions_x;

        // Copy chunk from global buffer to buffer for current kernel invocation
        // It's very important to handle margins accurately
        #ifdef READ_GLOBAL_MEM_ENABLE
        for (k = 0; k < PROC_REGION_SIZE_Y + 2; k++)
        {
            const uint8_t * src_ptr = &input_image[im_idx(WIDTH,
                                       region_idx_x * PROC_REGION_SIZE_X,
                                       region_idx_y * PROC_REGION_SIZE_Y + k)];
            #ifdef ALIGNED_MEMORY_ACCESS
            const uint8_t * aligned_src_ptr = src_ptr - (uint32_t)src_ptr % MEM_ALIGNMENT;
            const size_t transfer_size = (PROC_REGION_SIZE_X + 2) * sizeof(int8_t);
            const size_t transfer_size_aligned = transfer_size + (src_ptr - aligned_src_ptr) +
                                                 MEM_ALIGNMENT - (uint32_t)(src_ptr + transfer_size - 1) % MEM_ALIGNMENT - 1;
            uint8_t input_buffer_aligned[(PROC_REGION_SIZE_X + 2) + 2 * MEM_ALIGNMENT] __attribute__ ((aligned (MEM_ALIGNMENT)));
            //uint8_t * input_buffer = &input_buffer_aligned[(uint32_t)src_ptr % MEM_ALIGNMENT];
            
            malt_memcpy(
                (void *) input_buffer_aligned,
                (void *) aligned_src_ptr,
                transfer_size_aligned
            );
            malt_memcpy(
                (void *) (&input_buffer[im_idx(PROC_REGION_SIZE_X + 2, 0, k)]),
                (void *) &input_buffer_aligned[(uint32_t)src_ptr % MEM_ALIGNMENT],
                transfer_size
            );
            
            #else // ALIGNED_MEMORY_ACCESS
            malt_memcpy(
                (void *) (&input_buffer[im_idx(PROC_REGION_SIZE_X + 2, 0, k)]),
                (void *) src_ptr,
                (PROC_REGION_SIZE_X + 2) * sizeof(int8_t)
            );
            #endif // ALIGNED_MEMORY_ACCESS
        }
        #endif // READ_GLOBAL_MEM_ENABLE
        
        // Process image
        #ifdef PROCESS_DATA_ENABLE
        for (i = 1; i < PROC_REGION_SIZE_Y + 1; i++)
        {
            for (j = 1; j < PROC_REGION_SIZE_X + 1; j++)
            {
                output_buffer[im_idx(PROC_REGION_SIZE_X, j - 1, i - 1)] = apply_sobel_to_point_optimized_div((uint8_t *)input_buffer, (PROC_REGION_SIZE_X + 2), j, i);
            }
        }
        #endif
        
        // Copy processed chunks to global buffer
        #ifdef WRITE_GLOBAL_MEM_ENABLE
        for (k = 0; k < PROC_REGION_SIZE_Y; k++)
        {
            malt_memcpy(
               (void*) (&output_image[im_idx(WIDTH - 2, 
                                             region_idx_x * PROC_REGION_SIZE_X, 
                                             region_idx_y * PROC_REGION_SIZE_Y + k)]),
               (void*) (&output_buffer[im_idx(PROC_REGION_SIZE_X, 0, k)]),
                PROC_REGION_SIZE_X * sizeof(uint8_t)
            );
        }
        #endif
    }
    
    return 0;
}

////////  Master functions  ///////////

int proces_image_on_slaves(uint8_t * const input_image, uint8_t * const output_image, int num_slaves, result_t *results_row)
{
    //lwlog_info("Process image %s on %d slaves...", IMAGE_NAME, num_slaves);
    
    uint64_t t_start_us, t_stop_us;
    uint64_t imem_start_cnt, dmem_start_cnt, total_start_cnt;
    uint64_t imem_stop_cnt, dmem_stop_cnt, total_stop_cnt;
    
    malt_read_pcnt(&imem_start_cnt, &dmem_start_cnt, &total_start_cnt);
    t_start_us = malt_get_time_us();
    malt_start_pcnt();

    // Launch tasks on slaves
    for (int i = 0; i < num_slaves; i++)
    {
        int slave_id = malt_start_thr(slave_proces_image, input_image, output_image, i, num_slaves);
        if (slave_id == -1)
        {
            lwlog_err("Faild to launch task %d", i);
            exit(-1);
        }
    }
    malt_sm_wait_all_lines_free();
    
    // Processing finished
    malt_stop_pcnt();
    t_stop_us = malt_get_time_us();
    //lwlog_info("Processing done!");
    
    
    malt_read_pcnt(&imem_stop_cnt, &dmem_stop_cnt, &total_stop_cnt); 
    results_row[num_slaves - 1].imem_cnt = (imem_stop_cnt - imem_start_cnt);
    results_row[num_slaves - 1].dmem_cnt = (dmem_stop_cnt - dmem_start_cnt);
    results_row[num_slaves - 1].total_cnt = (total_stop_cnt - total_start_cnt);
    results_row[num_slaves - 1].time_us = (uint32_t)(t_stop_us - t_start_us);    
    
    // Проверка корректности результата
    //lwlog_info("Check CRC...");
    uint32_t crc = 0;
    #ifdef CHECK_CRC
    crc = crc32(0, output_image, (WIDTH - 2) * (HEIGHT - 2));
    #endif
    bool crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }
    results_row[num_slaves - 1].crc = crc_check_ok;

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Sobel results ###\n");
    sprintf(&results[strlen(results)], "Image: %s\n", IMAGE_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)results_row[num_slaves - 1].time_us) / (1000));
    //sprintf(&results[strlen(results)], "Iterations_done: 1\n");
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment:");
    #ifdef READ_GLOBAL_MEM_ENABLE
    sprintf(&results[strlen(results)], " READ_GLOBAL_MEM_ENABLE");
    #endif
    #ifdef PROCESS_DATA_ENABLE
    sprintf(&results[strlen(results)], " PROCESS_DATA_ENABLE");
    #endif 
    #ifdef WRITE_GLOBAL_MEM_ENABLE
    sprintf(&results[strlen(results)], " WRITE_GLOBAL_MEM_ENABLE");
    #endif
    sprintf(&results[strlen(results)], "\n");
    sprintf(&results[strlen(results)], "Imem_cnt_e3: %d\n", (uint32_t)((imem_stop_cnt - imem_start_cnt) / 1000));
    sprintf(&results[strlen(results)], "Dmem_cnt_e3: %d\n", (uint32_t)((dmem_stop_cnt - dmem_start_cnt) / 1000));
    sprintf(&results[strlen(results)], "Total_cnt_e3: %d\n", (uint32_t)((total_stop_cnt - total_start_cnt) / 1000));
    sprintf(&results[strlen(results)], "\n");
    //printf(results);
    printf("Num slaves = %d\n", num_slaves);

    printf("\n");

    return 0;
}



int main(void) 
{
    malt_print_system_config();

#ifdef MOVE_GLOBAL_BUFFERS_TO_HEAP
    // This works only with export MALT_LIBC=newlib in Makefile
    uint8_t * input_image = (uint8_t *)memalign(16, WIDTH * HEIGHT * sizeof(uint8_t) + MEM_ALIGNMENT * 2);
    uint8_t * output_image = (uint8_t *)memalign(16, (WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t) + MEM_ALIGNMENT * 2);

    memcpy(input_image, input_image_temp, WIDTH * HEIGHT * sizeof(uint8_t));
#else
    uint8_t *const input_image = input_image_temp;
    uint8_t *const output_image = output_image_temp;
#endif // MOVE_GLOBAL_BUFFERS_TO_HEAP

    if (((uint32_t)input_image % MEM_ALIGNMENT == 0) && ((uint32_t)output_image % MEM_ALIGNMENT == 0))
    {
        lwlog_info("Alignment OK: %x %x", (uint32_t)input_image, (uint32_t)output_image);
    }
    else
    {
        lwlog_err("Global buffers are not aligned: %x %x", (uint32_t)input_image, (uint32_t)output_image);
    }


    int num_slaves = malt_get_total_slaves();
    int super_master_id = malt_cur_core_num();
    lwlog_info("%d slaves found. Super master id: %x", num_slaves, super_master_id);

    result_t *results;
    results = malloc(num_slaves * sizeof(result_t));
    memset(results, 0, num_slaves * sizeof(result_t));
    
    for (int i = 0; i < num_slaves; i++)
    {
        proces_image_on_slaves(input_image, output_image, i + 1, results);
    }
    
    num_slaves = malt_get_total_slaves();
    proces_image_on_slaves(input_image, output_image, num_slaves, results);
    
    lwlog_info("Done!");
    
    printf("N\tdmem_cnt\ttotal_cnt\ttime_us\tcrc\n");
    for (int i = 0; i < num_slaves; i++)
    {
        printf("%d\t%d\t%d\t%d\t%x\n", 
               i + 1,
               results[i].dmem_cnt, 
               results[i].total_cnt, 
               results[i].time_us, 
               results[i].crc);
    }
    
    free(results);
    
#ifdef MOVE_GLOBAL_BUFFERS_TO_HEAP
    //free(input_image);
    //free(output_image);
#endif // MOVE_GLOBAL_BUFFERS_TO_HEAP
    return 0;
}

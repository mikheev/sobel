#ifndef __MALT_EXT_HEADER
#define __MALT_EXT_HEADER

// malt stdlib doesn't have abs() implementation
inline int abs(int __x) {return __x >= 0 ? __x : -__x;}

// Struct for malt calculation statistics
typedef struct{
    int imem_cnt;
    int dmem_cnt;
    int total_cnt;
    int time_us;
    int crc;
} result_t;

#endif // __MALT_EXT_HEADER
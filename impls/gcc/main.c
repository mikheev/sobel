#include "stdio.h"
#include "stdbool.h"
#include "string.h"
#include "time.h"

#include "../../common/sobel_core.h"
#include "../../common/crc_check.h"

#define LOG_COLOR (0)
#include "../../common/lwlog.h"

//#include "../../images/squares_130.h"
#include "../../images/tram_1026.h"

#define NUM_ITERATIONS 100

int main(void) 
{
    uint8_t input_image_src[] = IMAGE_SRC;
    uint8_t *input_image, *output_image;

    clock_t t_start, t_end;
    uint32_t iteration;

    // Все вычисденя должны производиться над данными из кучи. 
    // Результаты должны быть записаны туда же.
    // Поэтому нужно выделить соответствующие буфера и скопировать туда изображение.
    input_image = (uint8_t *) malloc(WIDTH * HEIGHT * sizeof(uint8_t));
    output_image = (uint8_t *) malloc((WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t));
    memcpy(input_image, input_image_src, WIDTH * HEIGHT * sizeof(uint8_t));

    //---------- Обработка изображения ------------
    lwlog_info("Process image %s...", IMAGE_NAME);
    t_start = clock();
    for (iteration = 0; iteration < NUM_ITERATIONS; iteration++)
    {
        int x, y;
        for (y = 0; y < (HEIGHT - 2); y++)
        {
            for (x = 0; x < (WIDTH - 2); x++)
            {
                output_image[im_idx((WIDTH - 2), x, y)] = apply_sobel_to_point_optimized_div(input_image, WIDTH, (x + 1), (y + 1));
                //output_image[im_idx((WIDTH - 2), x, y)] = apply_sobel_to_point_optimized_div_2(input_image, WIDTH, (x + 1), (y + 1));
                //output_image[im_idx((WIDTH - 2), x, y)] = apply_sobel_to_point(input_image, WIDTH, (x + 1), (y + 1));
            }
        }
    }
    t_end = clock();
    lwlog_info("Processing done!");
    //---------- Конец обработки изображения ------------

    // Проверка корректности результата
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, output_image, (WIDTH - 2) * (HEIGHT - 2));
    bool crc_check_ok = (crc == RESULT_CRC32);

    int x, y;
    // for (y = 0; y < (HEIGHT - 2); y++)
    // {
        // for (x = 0; x < (WIDTH - 2); x++)
        // {
            // printf("%.2x", input_image[im_idx((WIDTH), x, y)]);
        // }
        // printf("\n");
    // }
    // printf("\n");
    
    // for (y = 0; y < (HEIGHT - 2); y++)
    // {
        // for (x = 0; x < (WIDTH - 2); x++)
        // {
            // printf("%.2x", output_image[im_idx((WIDTH - 2), x, y)]);
        // }
        // printf("\n");
    // }
    

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc, RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Sobel results ###\n");
    sprintf(&results[strlen(results)], "Image: %s\n", IMAGE_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)(t_end - t_start) * 1000) / (NUM_ITERATIONS * CLOCKS_PER_SEC));
    sprintf(&results[strlen(results)], "Iterations_done: %d\n", NUM_ITERATIONS);
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment: \n");
    sprintf(&results[strlen(results)], "\n");
    printf(results);

    free(input_image);
    free(output_image);
    
    
    return 0;
}
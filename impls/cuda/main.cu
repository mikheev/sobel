#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"

#include "../../common/sobel_core.h"
#include "../../common/crc_check.h"

#define LOG_COLOR (0)
#include "../../common/lwlog.h"

#define INPUT_IMAGE_FILE "../../images/tram_1026.h"

#define NUM_ITERATIONS 100

#define WIDTH width
#define HEIGHT height
#define IMAGE_NAME image_name
#define RESULT_CRC32 result_crc32

// Calculation settings
#define READ_GLOBAL_MEM_ENABLE
#define PROCESS_DATA_ENABLE
#define WRITE_GLOBAL_MEM_ENABLE
#define CHECK_CRC

#define __func__ ""

__global__ void sobelKernel(uint8_t *input_image, uint8_t *output_image, const int16_t height, const int16_t width)
{
    uint32_t x = threadIdx.x;
    uint32_t y;
    
    for (y = 0; y < (HEIGHT - 2); y++)
    {
        output_image[im_idx((WIDTH - 2), x, y)] = apply_sobel_to_point_optimized_div(input_image, WIDTH, (x + 1), (y + 1));
    }
}



int main()
{
    uint8_t *input_image, *output_image;

    clock_t t_start, t_end;
    uint32_t iteration;
    cudaError_t cudaStatus;

    // --------- Сбор информации об устройстве
    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        lwlog_err("cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?\n");
        return -1;
    }

    cudaDeviceProp device_prop;
    cudaGetDeviceProperties(&device_prop, 0);
    printf("####################################################\n");
    printf("Device name:                %s\n", device_prop.name);
    printf("Major revision number:      %d\n", device_prop.major);
    printf("Minor revision Number:      %d\n", device_prop.minor);
    printf("Total Global Memory:        %ld\n", device_prop.totalGlobalMem);
    printf("Total shared mem per block: %ld\n", device_prop.sharedMemPerBlock);
    printf("Total const mem size:       %ld\n", device_prop.totalConstMem);
    printf("Warp size:                  %d\n", device_prop.warpSize);
    printf("Maximum block dimensions:   %d x %d x %d\n", device_prop.maxThreadsDim[0], \
                                                         device_prop.maxThreadsDim[1], \
                                                         device_prop.maxThreadsDim[2]);
    printf("Maximum grid dimensions:    %d x %d x %d\n", device_prop.maxGridSize[0], \
                                                        device_prop.maxGridSize[1], \
                                                        device_prop.maxGridSize[2]);
    printf("Clock Rate:                 %d\n", device_prop.clockRate);
    printf("Number of muliprocessors:   %d\n", device_prop.multiProcessorCount);
    printf("Number of muliprocessors:   %d\n", device_prop.multiProcessorCount);
    printf("####################################################\n");

    // --------- Загрузка данных 
    FILE *input_file;
    char str[256];
    char image_name[256];
    int width;
    int height;
    int result_crc32;
    int result_code;

    input_file = fopen(INPUT_IMAGE_FILE, "r");
    result_code = fscanf(input_file, "%s %s %s %s\n", str, str, str, str);
    result_code = fscanf(input_file, "%s %s %d\n", str, str, &width);
    result_code = fscanf(input_file, "%s %s %d\n", str, str, &height);
    // Все вычисденя должны производиться над данными из кучи. 
    // Результаты должны быть записаны туда же.
    // Поэтому нужно выделить соответствующие буфера и скопировать туда изображение.
    input_image = (uint8_t *) malloc(WIDTH * HEIGHT * sizeof(uint8_t));
    output_image = (uint8_t *) malloc((WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t));

    result_code = fscanf(input_file, "%s %s {%hhd\n", str, str, input_image);
    for (int i = 1; i < width * height; i++)
    {
        result_code = fscanf(input_file, ",%hhd\n", &input_image[i]);
    }
    result_code = fscanf(input_file, "%s\n", str);
    result_code = fscanf(input_file, "%s %s %s\n", str, str, image_name);
    result_code = fscanf(input_file, "%s %s %x\n", str, str, &result_crc32);
    if (result_code != 0)
    {
        lwlog_err("Problems with input file. Result code: %d, expected 3", result_code);
    }
    fclose(input_file);

    //memcpy(input_image, input_image_src, WIDTH * HEIGHT * sizeof(uint8_t));
    // --------- Конец загрузки данных

    uint8_t *input_image_d;
    uint8_t *output_image_d;

    // Allocate GPU buffers for input image and result
    cudaStatus = cudaMalloc((void**)&input_image_d, WIDTH * HEIGHT * sizeof(uint8_t));
    if (cudaStatus != cudaSuccess) {
        printf("cudaMalloc failed!\n");
    }

    cudaStatus = cudaMalloc((void**)&output_image_d, (WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t));
    if (cudaStatus != cudaSuccess) {
        printf("cudaMalloc failed!\n");
    }


    //---------- Обработка изображения ------------
    lwlog_info("Process image %s...", IMAGE_NAME);
    t_start = clock();
    for (iteration = 0; iteration < NUM_ITERATIONS; iteration++)
    {
        // Copy input image from host memory to GPU buffers.
        #ifdef READ_GLOBAL_MEM_ENABLE
        cudaStatus = cudaMemcpy(input_image_d, input_image, WIDTH * HEIGHT * sizeof(uint8_t), cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) {
            printf("cudaMemcpy failed!\n");
        }
        #endif

        // Launch a kernel with one thread per column
        #ifdef PROCESS_DATA_ENABLE
        sobelKernel<<<1, (WIDTH - 2)>>>(input_image_d, output_image_d, HEIGHT, WIDTH);

        // Check for any errors launching the kernel
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) {
            printf("addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        }
    
        // cudaDeviceSynchronize waits for the kernel to finish, and returns
        // any errors encountered during the launch.
        cudaStatus = cudaDeviceSynchronize();
        if (cudaStatus != cudaSuccess) {
            printf("cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        }
        #endif

        // Copy result back to the host
        #ifdef WRITE_GLOBAL_MEM_ENABLE
        cudaStatus = cudaMemcpy(output_image, output_image_d, (HEIGHT - 2) * (WIDTH - 2) * sizeof(uint8_t), cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) {
            printf("cudaMemcpy failed!\n");
        }
        #endif
    }
    t_end = clock();
    //---------- Конец обработки изображения ------------

    // Проверка корректности результата
    lwlog_info("Check CRC...");
    uint32_t crc = 0;
    #ifdef CHECK_CRC
    crc = crc32(0, output_image, (WIDTH - 2) * (HEIGHT - 2));
    #endif
    bool crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Sobel results ###\n");
    sprintf(&results[strlen(results)], "Image: %s\n", IMAGE_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)(t_end - t_start) * 1000) / (NUM_ITERATIONS * CLOCKS_PER_SEC));
    sprintf(&results[strlen(results)], "Iterations_done: %d\n", NUM_ITERATIONS);
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment:");
    #ifdef READ_GLOBAL_MEM_ENABLE
    sprintf(&results[strlen(results)], " READ_GLOBAL_MEM_ENABLE");
    #endif
    #ifdef PROCESS_DATA_ENABLE
    sprintf(&results[strlen(results)], " PROCESS_DATA_ENABLE");
    #endif 
    #ifdef WRITE_GLOBAL_MEM_ENABLE
    sprintf(&results[strlen(results)], " WRITE_GLOBAL_MEM_ENABLE");
    #endif
    sprintf(&results[strlen(results)], "\n");
    sprintf(&results[strlen(results)], "\n");
    printf("%s", results);

    cudaFree(input_image_d);
    cudaFree(output_image_d);

    free(input_image);
    free(output_image);
    
    return 0;
}
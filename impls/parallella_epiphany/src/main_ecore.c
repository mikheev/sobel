#include <e_bsp.h>
#include "stdint.h"

#include "../../../common/sobel_core.h"
#include "common.h"


int slave_proces_image(uint8_t * const input_image, uint8_t * const output_image, int slave_id, int slaves_total)
{
    // Get slave id
    const int num_regions_x = (WIDTH - 2) / PROC_REGION_SIZE_X;
    const int num_regions_y = (HEIGHT - 2) / PROC_REGION_SIZE_Y;
    const int num_regions = num_regions_x * num_regions_y;
        
    
    // Create the two input vectors
    volatile uint8_t input_buffer[CHUNK_SIZE];
    volatile uint8_t output_buffer[CHUNK_SIZE];
    
    int i, j, k;
    int region_idx, region_idx_x, region_idx_y;
    
    for (region_idx = slave_id; region_idx < num_regions; region_idx += slaves_total)
    {
        region_idx_x = region_idx % num_regions_x;
        region_idx_y = region_idx / num_regions_x;

        // Copy chunk from global buffer to buffer for current kernel invocation
        // It's very important to handle margins accurately
        #ifdef ECORE_READ_SHARED_MEM_ENABLE
        for (k = 0; k < PROC_REGION_SIZE_Y + 2; k++)
        {
            const uint8_t * src_ptr = &input_image[im_idx(WIDTH,
                                       region_idx_x * PROC_REGION_SIZE_X,
                                       region_idx_y * PROC_REGION_SIZE_Y + k)];

            ebsp_memcpy(
                (void *) (&input_buffer[im_idx(PROC_REGION_SIZE_X + 2, 0, k)]),
                (void *) src_ptr,
                (PROC_REGION_SIZE_X + 2) * sizeof(int8_t)
            );
        }
        #endif  // ECORE_READ_SHARED_MEM_ENABLE
        
        // Process image
        #ifdef PROCESS_DATA_ENABLE
        for (i = 1; i < PROC_REGION_SIZE_Y + 1; i++)
        {
            for (j = 1; j < PROC_REGION_SIZE_X + 1; j++)
            {
                output_buffer[im_idx(PROC_REGION_SIZE_X, j - 1, i - 1)] = apply_sobel_to_point_optimized_div((uint8_t *)input_buffer, (PROC_REGION_SIZE_X + 2), j, i);
            }
        }
        #endif  // PROCESS_DATA_ENABLE
        
        // Copy processed chunks to global buffer
        #ifdef ECORE_WRITE_SHARED_MEM_ENABLE
        for (k = 0; k < PROC_REGION_SIZE_Y; k++)
        {
            ebsp_memcpy(
               (void*) (&output_image[im_idx(WIDTH - 2, 
                                             region_idx_x * PROC_REGION_SIZE_X, 
                                             region_idx_y * PROC_REGION_SIZE_Y + k)]),
               (void*) (&output_buffer[im_idx(PROC_REGION_SIZE_X, 0, k)]),
                PROC_REGION_SIZE_X * sizeof(uint8_t)
            );
        }
        #endif  // ECORE_WRITE_SHARED_MEM_ENABLE
    }
    
    return 0;
}


int main() {
    uint8_t *input_image = (uint8_t *)(EXT_MEM_BASE + EXT_MEM_INPUT_DATA_OFFSET);
    uint8_t *output_image = (uint8_t *)(EXT_MEM_BASE + EXT_MEM_OUTPUT_DATA_OFFSET);
    
    bsp_begin();

    slave_proces_image(input_image, output_image, bsp_pid(), bsp_nprocs());

    bsp_end();

    return 0;
}
#define _POSIX_C_SOURCE 199309L

#include <host_bsp.h>
#include <stdio.h>
#include "common.h"
#include "stdlib.h"
#include "e-hal.h"
#include "time.h"

#include "../../../common/sobel_core.h"
#include "../../../common/crc_check.h"
#include "../../../common/lwlog.h"


uint8_t input_image_temp[] = IMAGE_SRC;
uint8_t output_image_temp[(WIDTH - 2) * (HEIGHT - 2)] = {1};
float times_ms[32];


void print_output_image(uint8_t *output_image)
{
    for (int i = 0; i < HEIGHT - 2; i++)
    {
        for (int j = 0; j < WIDTH - 2; j++)
        {
            printf("%.2x", output_image[i*(WIDTH - 2) + j]);
        }
        printf("\n");
    }
}


static inline float get_time_ms(void) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    return (float) t.tv_sec * 1000. + (float) t.tv_nsec / 1000000.;
}

int proces_image_on_slaves(int nprocs)
{
    lwlog_info("Process image on %d cores", nprocs);

    float t_start_ms, t_end_ms;
    float time_ms;
    e_mem_t ext_mem_input_image, ext_mem_output_image;

    bsp_init("main_ecore.elf", 0, NULL);
    bsp_begin(nprocs);

    e_alloc(&ext_mem_input_image, EXT_MEM_INPUT_DATA_OFFSET, WIDTH * HEIGHT * sizeof(uint8_t));
    e_alloc(&ext_mem_output_image, EXT_MEM_OUTPUT_DATA_OFFSET, WIDTH * HEIGHT * sizeof(uint8_t));


    lwlog_info("Launch calculation");
    t_start_ms = get_time_ms();

    #ifdef HOST_WRITE_SHARED_MEM_ENABLE
    e_write(&ext_mem_input_image, 0, 0, 0, (void *)input_image_temp, WIDTH * HEIGHT * sizeof(uint8_t));
    #endif  // HOST_WRITE_SHARED_MEM_ENABLE

    ebsp_spmd();

    #ifdef HOST_READ_SHARED_MEM_ENABLE
    e_read(&ext_mem_output_image, 0, 0, 0, (void *)output_image_temp, (WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t));
    #endif  // HOST_READ_SHARED_MEM_ENABLE

    t_end_ms = get_time_ms();
    time_ms = t_end_ms - t_start_ms;
    lwlog_info("Processing done! Time spent: %.2f ms", time_ms);
    times_ms[nprocs] = time_ms;

    bsp_end(nprocs);


    #ifdef CHECK_CRC
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, output_image_temp, (WIDTH - 2) * (HEIGHT - 2));
    int crc_check_ok = (crc == RESULT_CRC32);
    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }
    #endif  // CHECK_CRC

    //print_output_image(output_image_temp);

    return 0;
}

int main(int argc, char** argv)
{

    int num_slaves = 16;
    lwlog_info("%d cores available", num_slaves);
    //proces_image_on_slaves(num_slaves);

    for (int i = 1; i <= num_slaves; i++)
    {
        proces_image_on_slaves(i);
    }

    for (int i = 1; i <= num_slaves; i++)
    {
        printf("%.2f\n", times_ms[i]);
    }


    return 0;
}
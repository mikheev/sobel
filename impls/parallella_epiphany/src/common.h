#ifndef COMMON_H_
#define COMMON_H_

#define EXT_MEM_BASE (0x8e000000)
#define EXT_MEM_INPUT_DATA_OFFSET  (0x00100000)
#define EXT_MEM_OUTPUT_DATA_OFFSET (0x01000000)

#define PROC_REGION_SIZE_X 16
#define PROC_REGION_SIZE_Y 16

#define CHUNK_SIZE      ((PROC_REGION_SIZE_X + 2) * (PROC_REGION_SIZE_Y + 2))   // Words per processing element


// Calculation settings
#define HOST_WRITE_SHARED_MEM_ENABLE
#define ECORE_READ_SHARED_MEM_ENABLE
#define PROCESS_DATA_ENABLE
#define ECORE_WRITE_SHARED_MEM_ENABLE
#define HOST_READ_SHARED_MEM_ENABLE
#define CHECK_CRC


//#include "../../../images/squares_130.h"
#include "../../../images/tram_1026.h"

#endif  // COMMON_H_

//#include "../../common/sobel_core.h"

__left uint32_t left_mem[200]   = {0xaa};
__right uint32_t right_mem[200] = {0xbb};
__left uint32_t i = 0;
__right uint32_t i_r = 0;


#define PROC_REGION_SIZE_X 16
#define PROC_REGION_SIZE_Y 16

#define WIDTH_WORDS_M ((PROC_REGION_SIZE_X + 4) / 4)

#define LEFT_MEMORY_OFFSET_WORDS 2

#define INPUT_IMAGE_BASE  ((PROC_REGION_SIZE_X + 4) / 4 * 2)
#define OUTPUT_IMAGE_BASE (INPUT_IMAGE_BASE + (PROC_REGION_SIZE_X + 4) * (PROC_REGION_SIZE_Y + 2) / 4)

// Kernel size
#define K_SIZE_X    3
#define K_SIZE_Y    3

/*  -- X kernel --
 *  | -1, 0, 1 |
 *  | -2, 0, 2 |
 *  | -1, 0, 1 |
 */
#define K_X_00  (-1)
#define K_X_01  ( 0)
#define K_X_02  ( 1)
#define K_X_10  (-2)
#define K_X_11  ( 0)
#define K_X_12  ( 2)
#define K_X_20  (-1)
#define K_X_21  ( 0)
#define K_X_22  ( 1)

/*  -- Y kernel --
 * |-1, -2, -1 |
 * | 0,  0,  0 |
 * | 1,  2,  1 |
 */
#define K_Y_00  (-1)
#define K_Y_01  (-2)
#define K_Y_02  (-1)
#define K_Y_10  ( 0)
#define K_Y_11  ( 0)
#define K_Y_12  ( 0)
#define K_Y_20  ( 1)
#define K_Y_21  ( 2)
#define K_Y_22  ( 1)

#define abs_16(x) (((x) * ((~(x) & 0x8000) >> 15) -    \
                    (x) * (( (x) & 0x8000) >> 15)) & 0xffff)

static inline uint32_t mult_16_by_8(uint32_t a, uint32_t b)
{
    volatile uint32_t a_p = (a & 0xff00) >> 8;
    volatile uint32_t a_m = a & 0xff;
    volatile uint32_t b_p = (b & 0xff00) >> 8;
    volatile uint32_t b_m = b & 0xff;
    volatile uint32_t result = a_p * b_p;
    //.result = a_p * b_p;
    result = result << 16;
    //return ((a_p * b_p) << 16) + ((a_m * b_p + a_p * b_m) << 8) + a_m * b_m;
    result += ((a_m * b_p + a_p * b_m) << 8) + a_m * b_m;
    return result;
}


static inline uint32_t apply_sobel_to_point_32_left(uint32_t * image, uint32_t width_words, uint32_t x, uint32_t y)
{
    uint32_t result = 0;
    int32_t temp_result_1;
    int32_t temp_result_2;
    
    // 0x000000vv
    // x - 1: [x    ] & 0x0000ff00 >> 8
    // x    : [x    ] & 0x000000ff >> 0
    // x + 1: [x + 1] & 0xff000000 >> 24
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_01 +
        (int)((image[(y - 1) * width_words + x + 1] & 0xff000000) >> 24) * K_X_02 +
        (int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_X_11 +
        (int)((image[(y    ) * width_words + x + 1] & 0xff000000) >> 24) * K_X_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_21 +
        (int)((image[(y + 1) * width_words + x + 1] & 0xff000000) >> 24) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
        

    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;
    
    //result |= (abs(temp_result_1) + abs(temp_result_2)) / 6;
    result |= ((temp_result_1 + temp_result_2) << 0);

    // 0x0000vv00
    // x - 1: [x    ] & 0x00ff0000 >> 16
    // x    : [x    ] & 0x0000ff00 >> 8
    // x + 1: [x    ] & 0x000000ff << 0
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_02 +
        (int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_11 +
        (int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_X_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16)* K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
        
    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;

    //result |= ((abs(temp_result_1) + abs(temp_result_2)) / 6) << 8;
        result |= ((temp_result_1 + temp_result_2) << 8);

    // 0x00vv0000
    // x - 1: [x    ] & 0xff000000 >> 24
    // x    : [x    ] & 0x00ff0000 >> 16
    // x + 1: [x    ] & 0x0000ff00 >> 8
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_02 +
        (int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_11 +
        (int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24)* K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
    
    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;
    
    //result |= ((abs(temp_result_1) + abs(temp_result_2)) / 6) << 16;
    result |= ((temp_result_1 + temp_result_2) << 16);
      
    // 0xvv000000
    // x - 1: [x - 1] & 0x000000ff >> 0
    // x    : [x    ] & 0xff000000 >> 24
    // x + 1: [x    ] & 0x00ff0000 >> 16
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_02 +
       (int32_t)((image[(y    ) * width_words + x - 1] & 0x000000ff)) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_X_11 +
        (int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_12 +
        (int)((image[(y + 1) * width_words + x - 1] & 0x000000ff) >> 0)* K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
        
    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;
    
    // result |= ((abs(temp_result_1) + abs(temp_result_2)) / 6) << 24;
    result |= ((temp_result_1 + temp_result_2) << 24);
    
    return result;
}


static inline uint32_t apply_sobel_to_point_32_right(__attribute__((address_space(1))) uint32_t * image, uint32_t width_words, uint32_t x, uint32_t y)
{
    uint32_t result = 0;
    int32_t temp_result_1;
    int32_t temp_result_2;
    
    // 0x000000vv
    // x - 1: [x    ] & 0x0000ff00 >> 8
    // x    : [x    ] & 0x000000ff >> 0
    // x + 1: [x + 1] & 0xff000000 >> 24
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_01 +
        (int)((image[(y - 1) * width_words + x + 1] & 0xff000000) >> 24) * K_X_02 +
        (int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_X_11 +
        (int)((image[(y    ) * width_words + x + 1] & 0xff000000) >> 24) * K_X_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_21 +
        (int)((image[(y + 1) * width_words + x + 1] & 0xff000000) >> 24) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
        

    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x + 1] & 0xff000000) >> 24) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;
    
    //result |= (abs(temp_result_1) + abs(temp_result_2)) / 6;
    result |= ((temp_result_1 + temp_result_2) << 0);

    // 0x0000vv00
    // x - 1: [x    ] & 0x00ff0000 >> 16
    // x    : [x    ] & 0x0000ff00 >> 8
    // x + 1: [x    ] & 0x000000ff << 0
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_02 +
        (int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_11 +
        (int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_X_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16)* K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
        
    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x000000ff) >> 0) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;

    //result |= ((abs(temp_result_1) + abs(temp_result_2)) / 6) << 8;
        result |= ((temp_result_1 + temp_result_2) << 8);

    // 0x00vv0000
    // x - 1: [x    ] & 0xff000000 >> 24
    // x    : [x    ] & 0x00ff0000 >> 16
    // x + 1: [x    ] & 0x0000ff00 >> 8
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_02 +
        (int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_11 +
        (int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24)* K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
    
    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x0000ff00) >> 8) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;
    
    //result |= ((abs(temp_result_1) + abs(temp_result_2)) / 6) << 16;
    result |= ((temp_result_1 + temp_result_2) << 16);
      
    // 0xvv000000
    // x - 1: [x - 1] & 0x000000ff >> 0
    // x    : [x    ] & 0xff000000 >> 24
    // x + 1: [x    ] & 0x00ff0000 >> 16
    temp_result_1 = (
        (int)((image[(y - 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_X_00 +
      //(int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_02 +
       (int32_t)((image[(y    ) * width_words + x - 1] & 0x000000ff)) * K_X_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_X_11 +
        (int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_12 +
        (int)((image[(y + 1) * width_words + x - 1] & 0x000000ff) >> 0)* K_X_20 +
      //(int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_X_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_X_22
        );
    //temp_result_1 = mult_16_by_8(abs_16(temp_result_1), 0x5556) >> 17;
    temp_result_1 = (abs_16(temp_result_1) * 0x5556) >> 17;
        
    temp_result_2 = (
        (int)((image[(y - 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_00 +
        (int)((image[(y - 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_01 +
        (int)((image[(y - 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_02 +
      //(int)((image[(y    ) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_10 +
      //(int)((image[(y    ) * width_words + x    ] & 0xff000000) >> 24) * K_Y_11 +
      //(int)((image[(y    ) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_12 +
        (int)((image[(y + 1) * width_words + x - 1] & 0x000000ff) >> 0) * K_Y_20 +
        (int)((image[(y + 1) * width_words + x    ] & 0xff000000) >> 24) * K_Y_21 +
        (int)((image[(y + 1) * width_words + x    ] & 0x00ff0000) >> 16) * K_Y_22
        );
    //temp_result_2 = mult_16_by_8(abs_16(temp_result_2), 0x5556) >> 17;
    temp_result_2 = (abs_16(temp_result_2) * 0x5556) >> 17;
    
    // result |= ((abs(temp_result_1) + abs(temp_result_2)) / 6) << 24;
    result |= ((temp_result_1 + temp_result_2) << 24);
    
    return result;
}

void process_image_in_left_mem()
{
    i = 0;
    for (int k = 0; k < (PROC_REGION_SIZE_Y + 2) * WIDTH_WORDS_M; k++)
    {
        // // Simple pattern
        // *(left_mem + OUTPUT_IMAGE_BASE + i) = 0xabcdffff;

        // // Copy
        // *(left_mem + OUTPUT_IMAGE_BASE + i) = *(left_mem + INPUT_IMAGE_BASE + i);

        // Processing
        // Warning!
        // Division here will be made by preprocessor.
        // Don’t try to use division in runtime. It is not supported.
        *(left_mem + OUTPUT_IMAGE_BASE + i) =
             apply_sobel_to_point_32_left((left_mem + INPUT_IMAGE_BASE), WIDTH_WORDS_M, i % WIDTH_WORDS_M, i / WIDTH_WORDS_M);

        // // Indexing
        // *(left_mem + INPUT_IMAGE_BASE + i) = (i / WIDTH_WORDS_M) << 8 | i % WIDTH_WORDS_M;;
        // *(left_mem + OUTPUT_IMAGE_BASE + i) / 4 + j) = (i / WIDTH_WORDS_M) << 8 | i % WIDTH_WORDS_M;

        i++;
    }
}

void process_image_in_right_mem()
{
    i = 0;
    for (int k = 0; k < (PROC_REGION_SIZE_Y + 2) * WIDTH_WORDS_M; k++)
    {
        // // Simple pattern
        // *(right_mem + OUTPUT_IMAGE_BASE + i) = 0xabcdffff;

        // // Copy
        // *(right_mem + OUTPUT_IMAGE_BASE + i) = *(right_mem + INPUT_IMAGE_BASE + i);

        // Processing
        // Warning!
        // Division here will be made by preprocessor.
        // Don’t try to use division in runtime. It is not supported.
        *(right_mem + OUTPUT_IMAGE_BASE + i) =
             apply_sobel_to_point_32_right((right_mem + INPUT_IMAGE_BASE), WIDTH_WORDS_M, i % WIDTH_WORDS_M, i / WIDTH_WORDS_M);

        // // Indexing
        // *(right_mem + INPUT_IMAGE_BASE + i) = (i / WIDTH_WORDS_M) << 8 | i % WIDTH_WORDS_M;
        // *(right_mem + OUTPUT_IMAGE_BASE + i) / 4 + j) = (i / WIDTH_WORDS_M) << 8 | i % WIDTH_WORDS_M;

        i++;
    }
}

void copy_input_image_right(void)
{
    i = 0;
    for (int k = 0; k < (PROC_REGION_SIZE_Y + 2) * (PROC_REGION_SIZE_X + 4) / 4; k++)
    {
        *(right_mem + INPUT_IMAGE_BASE + i) = *(left_mem + INPUT_IMAGE_BASE + i);
        i++;
    }
}

void copy_input_image_left(void)
{
    i = 0;
    for (int k = 0; k < (PROC_REGION_SIZE_Y + 2) * (PROC_REGION_SIZE_X + 4) / 4; k++)
    {
        *(left_mem + INPUT_IMAGE_BASE + i) = *(right_mem + INPUT_IMAGE_BASE + i);
        i++;
    }
}

void copy_output_image_right(void)
{
    i = 0;
    for (int k = 0; k < (PROC_REGION_SIZE_Y + 2) * (PROC_REGION_SIZE_X + 4) / 4; k++)
    {
        *(right_mem + OUTPUT_IMAGE_BASE + i) = *(left_mem + OUTPUT_IMAGE_BASE + i);
        i++;
    }
}

void copy_output_image_left(void)
{
    i = 0;
    for (int k = 0; k < (PROC_REGION_SIZE_Y + 2) * (PROC_REGION_SIZE_X + 4) / 4; k++)
    {
        *(left_mem + OUTPUT_IMAGE_BASE + i) = *(right_mem + OUTPUT_IMAGE_BASE + i);
        i++;
    }
}


__kernel kernel()
{
    
}
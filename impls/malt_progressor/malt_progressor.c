#include <stdio.h>
#include <stdint.h>
#include <libmalt.h>
#include <stdint.h>
#include <stdlib.h>

#include <progressor/malt_progressor.h>
#include <progressor_control_ext.h>
#include <progressor_memory_ext.h>

#include "../../common/sobel_core.h"
#include "../../common/crc_check.h"
#include "../../common/lwlog.h"

#define PROC_REGION_SIZE_X 16
#define PROC_REGION_SIZE_Y 16

#define PGR_INPUT_IMAGE_BASE ((PROC_REGION_SIZE_X + 4) * 2)
#define PGR_OUTPUT_IMAGE_BASE (PGR_INPUT_IMAGE_BASE + (PROC_REGION_SIZE_X + 4) * (PROC_REGION_SIZE_Y + 2))

#define MEM_ALIGNMENT 16

// Memory allocation setup
//#define MOVE_GLOBAL_BUFFERS_TO_HEAP


// Image selection
//#include "../../images/squares_130.h" // To use this image in DOUBLE_SIDE_MODE
                                      // reduce REGION_SIZE_X to 8 (in host and device code)
#include "../../images/tram_1026.h"

// Calculation settings
// Warning!!!
// Copy (read / write) requires device_prepare(),
// which is under PROCESS_DATA. So run program with
// PROCESS_DATA_ENABLE first.
#define READ_GLOBAL_MEM_ENABLE
#define PROCESS_DATA_ENABLE
#define WRITE_GLOBAL_MEM_ENABLE
#define CHECK_CRC


// Side mode
//  :Left   : Right :
//  :side   : side  :
//  | X | X | X | X |
// SINGLE_SIDE_MODE is default. No definition needed.
#define DOUBLE_SIDE_MODE

// Internal columns
// external ----+-----------+
// internal ----(---+---+   |
//              |   |   |   |
//              V   V   V   V
//            | X | X | X | X |
#define INTERNAL_COLUMNS_ENABLED

#ifdef DOUBLE_SIDE_MODE
#define ACTIVE_PGR_SIDES 2
#else // SINGLE_SIDE_MODE
#define ACTIVE_PGR_SIDES 1
#endif

#ifdef INTERNAL_COLUMNS_ENABLED
#define ACTIVE_PGR_COLUMNS (2 * ACTIVE_PGR_SIDES)
#else // INTERNAL_COLUMNS_ENABLED
#define ACTIVE_PGR_COLUMNS ACTIVE_PGR_SIDES
#endif


uint8_t input_image_temp[] __attribute__ ((aligned (MEM_ALIGNMENT))) = IMAGE_SRC;
uint8_t output_image_temp[(WIDTH - 2) * (HEIGHT - 2)] __attribute__ ((aligned (MEM_ALIGNMENT))) = {1};

#ifndef MOVE_GLOBAL_BUFFERS_TO_HEAP
// Make linker not to place data in on-chip memory.
volatile uint8_t dummy_data_shifter[1000000] = {1};
#endif // MOVE_GLOBAL_BUFFERS_TO_HEAP

uint8_t sobel_pgr_kernel[] __attribute__ ((aligned (16))) = {
#include "sobel_kernel.c.asm.code.inc"
};

uint32_t sobel_pgr_data_left[] __attribute__ ((aligned (16))) = {
#include "sobel_kernel.c.asm.left.inc"
};

uint32_t sobel_pgr_data_right[] __attribute__ ((aligned (16))) = {
#include "sobel_kernel.c.asm.right.inc"
};

#define MAP(...)
#define SEG(...)
#define SYM(sym,val,...) sym=val,
enum syms {
#include  "sobel_kernel.c.asm.map.d"
};


static inline void download_region_to_progressor(uint8_t *input_image,
                                                 uint32_t base_region_idx_x,
                                                 uint32_t base_region_idx_y)
{
    uint32_t num_rows = malt_pgr_get_row_num();

    for (int k = 0; k < PROC_REGION_SIZE_Y + 2; k++)
    {
        uint32_t offset = PGR_INPUT_IMAGE_BASE + k * (PROC_REGION_SIZE_X + 4);

        // There is extra copys but it may be faster due to larger blocks
        // Make a test on a real hardware!!!

        // 1. Copy data from global memory to local microblaze buffer
        const uint8_t * src_ptr = &input_image[im_idx(WIDTH,
                                               base_region_idx_x * PROC_REGION_SIZE_X,
                                               base_region_idx_y * PROC_REGION_SIZE_Y + k)];
        const uint8_t * aligned_src_ptr = src_ptr - (uint32_t)src_ptr % MEM_ALIGNMENT;
        const size_t transfer_size = (PROC_REGION_SIZE_X * num_rows * ACTIVE_PGR_SIDES + 4) * sizeof(uint8_t);
        uint8_t input_buffer_aligned[(PROC_REGION_SIZE_X * num_rows * ACTIVE_PGR_SIDES + 4) + 2 * MEM_ALIGNMENT] __attribute__ ((aligned (MEM_ALIGNMENT)));

        const size_t transfer_size_aligned = transfer_size + (src_ptr - aligned_src_ptr) +
                                             MEM_ALIGNMENT - (uint32_t)(src_ptr + transfer_size - 1) % MEM_ALIGNMENT - 1;

        malt_memcpy(
            (void *) input_buffer_aligned,
            (void *) aligned_src_ptr,
            transfer_size_aligned
        );

        uint8_t * input_buffer = &input_buffer_aligned[(uint32_t)src_ptr % MEM_ALIGNMENT];

        // 2.1 Copy data from local memory to progressor left memory
        for (uint8_t row = 0; row < num_rows; row++)
        {
            pgr_mem_ext_write_row_edge_data(offset,
                                            (void*)(&input_buffer[PROC_REGION_SIZE_X * row]),
                                            (PROC_REGION_SIZE_X + 4),
                                            LEFT_EDGE_MEM,
                                            row);
        }

#ifdef DOUBLE_SIDE_MODE
        // 2.2 Copy data from local memory to progressor right memory
        for (uint8_t row = 0; row < num_rows; row++)
        {
            pgr_mem_ext_write_row_edge_data(offset,
                                            (void*)&input_buffer[PROC_REGION_SIZE_X * (num_rows + row)],
                                            (PROC_REGION_SIZE_X + 4),
                                            RIGHT_EDGE_MEM,
                                            row);
        }
#endif // COLUMN_MODE

        // ------- Copy method devider

        // // Second case it is more simple
        // for (uint8_t row = 0; row < num_rows; row++)
        // {
            // malt_memcpy(
                // (void *) malt_pgr_row_mem_ptr(row, offset, LEFT_EDGE_MEM),
                // (void *) (&input_image[im_idx(WIDTH,
                                              // (base_region_idx_x + row ) * PROC_REGION_SIZE_X,
                                               // base_region_idx_y * PROC_REGION_SIZE_Y + k)]),
                // (PROC_REGION_SIZE_X + 4) * sizeof(int8_t)
            // );
        // }
    }
}

static inline void process_part_mb_emulation(void)
{
    uint32_t num_rows = malt_pgr_get_row_num();
    for (int i = 0; i < PROC_REGION_SIZE_Y; i++)
    {
        for (int j = 0; j < PROC_REGION_SIZE_X; j++)
        {
            for (uint8_t row = 0; row < num_rows; row++)
            {
                *(uint8_t *)malt_pgr_row_mem_ptr(row, PGR_OUTPUT_IMAGE_BASE + (i + 1)* (PROC_REGION_SIZE_X + 4) + j + 1, LEFT_EDGE_MEM) =
                    *(uint8_t *)malt_pgr_row_mem_ptr(row, PGR_INPUT_IMAGE_BASE + (i + 1)* (PROC_REGION_SIZE_X + 4) + j + 1, LEFT_EDGE_MEM);
                *(uint8_t *)malt_pgr_row_mem_ptr(row, PGR_INPUT_IMAGE_BASE + (i + 1)* (PROC_REGION_SIZE_X + 4) + j + 1, LEFT_EDGE_MEM) = 0;
            }
        }
    }
}

static inline void device_prepare(void)
{
    for (uint32_t col = 0; col < malt_pgr_get_col_num(); col++)
    {
        for (uint32_t row = 0; row < malt_pgr_get_row_num(); row++)
        {
            for (uint32_t word = 0; word < sizeof(sobel_pgr_data_right) / sizeof(uint32_t); word++)
            {
                malt_pgr_write_row_ldmem(row, word * sizeof(uint32_t), sobel_pgr_data_right[word]);
            }
        }
        pgr_mem_shift_right();
        //pgr_mem_ext_a_print_memory(220, 3, MEM_OUTPUT_MODE_WORD_32_BIT);
    }

    for (uint32_t row = 0; row < malt_pgr_get_row_num(); row++)
    {
        for (uint32_t word = 0; word < sizeof(sobel_pgr_data_right) / sizeof(uint32_t); word++)
        {
            malt_pgr_write_row_ldmem(row, word * sizeof(uint32_t), sobel_pgr_data_right[word]);
        }
    }

    // Fill instruction memory
    malt_pgr_fill_instr_mem(sobel_pgr_kernel, sizeof(sobel_pgr_kernel));
    // malt_pgr_fill_instr_mem_for_column(0, sobel_pgr_kernel, sizeof(sobel_pgr_kernel));
    // malt_pgr_fill_instr_mem_for_column(1, sobel_pgr_kernel, sizeof(sobel_pgr_kernel));
    // malt_pgr_fill_instr_mem_for_column(2, sobel_pgr_kernel, sizeof(sobel_pgr_kernel));
    // malt_pgr_fill_instr_mem_for_column(3, sobel_pgr_kernel, sizeof(sobel_pgr_kernel));
}

static inline void process_part(void)
{
    // It is very important to launch cores with valid code and data.
    // Core launched without valid code can damage shared memory
#ifdef DOUBLE_SIDE_MODE
#ifdef INTERNAL_COLUMNS_ENABLED
    const uint32_t ena_mask = 0x1 | 0x2 | 0x4 | 0x8;
#else // INTERNAL_COLUMNS_ENABLED
    const uint32_t ena_mask = 0x1 | 0x8;
#endif // INTERNAL_COLUMNS_ENABLED
#else // SINGLE_SIDE_MODE
#ifdef INTERNAL_COLUMNS_ENABLED
    const uint32_t ena_mask = 0x1 | 0x2;
#else // INTERNAL_COLUMNS_ENABLED
    const uint32_t ena_mask = 0x1;
#endif // INTERNAL_COLUMNS_ENABLED
#endif // COLUMN_MODE

    malt_pgr_set_tramp(process_image_in_left_mem, 0, 0x1);
    malt_pgr_set_tramp(process_image_in_left_mem, 0, 0x2);
    malt_pgr_set_tramp(process_image_in_right_mem, 0, 0x4);
    malt_pgr_set_tramp(process_image_in_right_mem, 0, 0x8);
    malt_progressor_run(ena_mask);
}

static inline void shift_input_parts_in(void)
{
    //  ___         ___
    // /   V       V   \
    //|m| |m| |m| |m| |m|
    //
    // It is very important to launch cores with valid code and data.
    // Core launched without valid code can damage shared memory
#ifdef DOUBLE_SIDE_MODE
    const uint32_t ena_mask = 0x1 | 0x8;
#else // SINGLE_SIDE_MODE
    const uint32_t ena_mask = 0x1;
#endif // COLUMN_MODE
    malt_pgr_set_tramp(copy_input_image_right, 0, 0x1);
    malt_pgr_set_tramp(copy_input_image_left, 0, 0x8);
    malt_progressor_run(ena_mask);
}

static inline void shift_processed_parts_out(void)
{
    //  ___         ___
    // V   \       /   V
    //|m| |m| |m| |m| |m|
    //
    // It is very important to launch cores with valid code and data.
    // Core launched without valid code can damage shared memory
#ifdef DOUBLE_SIDE_MODE
    const uint32_t ena_mask = 0x1 | 0x8;
#else // SINGLE_SIDE_MODE
    const uint32_t ena_mask = 0x1;
#endif // COLUMN_MODE
    malt_pgr_set_tramp(copy_output_image_left, 0, 0x1);
    malt_pgr_set_tramp(copy_output_image_right, 0, 0x8);
    malt_progressor_run(ena_mask);
}

static inline void upload_region_to_gloal_memory(uint8_t * output_image,
                                                 uint32_t base_region_idx_x,
                                                 uint32_t base_region_idx_y)
{
    const uint32_t num_rows = malt_pgr_get_row_num();

    const size_t transfer_size = (PROC_REGION_SIZE_X * num_rows * ACTIVE_PGR_SIDES) * sizeof(uint8_t);
    volatile uint8_t output_buffer_aligned[transfer_size + MEM_ALIGNMENT] __attribute__ ((aligned (MEM_ALIGNMENT)));

    for (int k = 0; k < PROC_REGION_SIZE_Y; k++)
    {
        // Global memory:       ddddddddd
        //                |   |   |   |   |
        //                      ^^ - alignment offset
        //                      ^ - dst_ptr
        // Local memory:      00ddddddddd
        //                    |   |   |   |
        //                    ^^ - alignment padding
        //                    |_____________| - output_buffer_aligned
        //                      |___________| - output_buffer

        uint32_t offset = PGR_OUTPUT_IMAGE_BASE + (k + 1) * (PROC_REGION_SIZE_X + 4) + 1;
        const uint8_t * dst_ptr = &output_image[im_idx(WIDTH - 2,
                                                       base_region_idx_x * PROC_REGION_SIZE_X,
                                                       base_region_idx_y * PROC_REGION_SIZE_Y + k)];
        const uint32_t aligned_offset = MEM_ALIGNMENT - (uint32_t)(dst_ptr - 1) % MEM_ALIGNMENT - 1;
        const uint32_t  alignment_padding = (uint32_t)(dst_ptr) % MEM_ALIGNMENT;
        uint8_t * output_buffer = &output_buffer_aligned[alignment_padding];
        // output_buffer[PROC_REGION_SIZE_X * num_rows * ACTIVE_PGR_SIDES - 3] = 0xff;  // To avoid strange optimization
        // output_buffer[PROC_REGION_SIZE_X * num_rows * ACTIVE_PGR_SIDES - 2] = 0xff;  // To avoid strange optimization
        // output_buffer[PROC_REGION_SIZE_X * num_rows * ACTIVE_PGR_SIDES - 1] = 0xff;  // To avoid strange optimization
        size_t aligned_transfer_size = (transfer_size - aligned_offset) - (transfer_size - aligned_offset) % MEM_ALIGNMENT;


        // 1.1 Copy data from left progressor edge mem to local memory
        for (uint8_t row = 0; row < num_rows; row++)
        {
            pgr_mem_ext_read_row_edge_data((void*)&output_buffer[PROC_REGION_SIZE_X * row],
                                           offset,
                                           PROC_REGION_SIZE_X,
                                           LEFT_EDGE_MEM,
                                           row);
            //memset((void*)&output_buffer[PROC_REGION_SIZE_X * row], 0x12, PROC_REGION_SIZE_X);
        }

#ifdef DOUBLE_SIDE_MODE
        // 1.2 Copy data from right progressor edge mem to local memory
        for (uint8_t row = 0; row < num_rows; row++)
        {
            pgr_mem_ext_read_row_edge_data((void*)&output_buffer[PROC_REGION_SIZE_X * (num_rows + row)],
                                           offset,
                                           PROC_REGION_SIZE_X,
                                           RIGHT_EDGE_MEM,
                                           row);
        }
#endif // COLUMN_MODE

        // 2. Copy data from local memory to global
        // Unaligned data in the beginning
        malt_memcpy(
            (void *) dst_ptr,
            (void *) output_buffer,
            aligned_offset
        );
        // Main aligned transfer
        malt_memcpy(
            (void *) &dst_ptr[aligned_offset],
            (void *) &output_buffer[aligned_offset],
            aligned_transfer_size
        );
        // Unaligned data in the end
        malt_memcpy(
            (void *) &dst_ptr[aligned_offset + aligned_transfer_size],
            (void *) &output_buffer[aligned_offset + aligned_transfer_size],
            transfer_size - aligned_transfer_size - aligned_offset
        );

        // ------ Copy method devider

        // for (uint8_t row = 0; row < num_rows; row++)
        // {
            // malt_memcpy(
                // (void *) (&output_image[im_idx(WIDTH - 2,
                                              // (base_region_idx_x + row) * PROC_REGION_SIZE_X,
                                               // base_region_idx_y * PROC_REGION_SIZE_Y + k)]),
                // (void *) malt_pgr_row_mem_ptr(row, offset, LEFT_EDGE_MEM),
                // (PROC_REGION_SIZE_X) * sizeof(int8_t)
            // );
        // }
    }
}

void print_output_image(uint8_t *output_image)
{
    for (int i = 0; i < HEIGHT - 2; i++)
    {
        for (int j = 0; j < WIDTH - 2; j++)
        {
            printf("%.2x", output_image[i*(WIDTH - 2) + j]);
        }
        printf("\n");
    }
}

/**********************   Main test function   **********************/
void proces_image_on_core(uint8_t * const input_image, uint8_t * const output_image, int core_id, int cores_total)
{
    const int num_regions_x = (WIDTH - 2) / PROC_REGION_SIZE_X;
    const int num_regions_y = (HEIGHT - 2) / PROC_REGION_SIZE_Y;
    const int num_regions = num_regions_x * num_regions_y;
    const int pgr_num_rows = malt_pgr_get_row_num();


    for (int region_idx = pgr_num_rows * ACTIVE_PGR_COLUMNS * core_id;
             region_idx < num_regions;
             region_idx += (pgr_num_rows * ACTIVE_PGR_COLUMNS * cores_total))
    {
        int region_idx_x = region_idx % num_regions_x;
        int region_idx_y = region_idx / num_regions_x;

        //printf("Process region %d\t%d\t(%d %%)\n", region_idx_x, region_idx_y, region_idx * 100 / num_regions);

#ifdef READ_GLOBAL_MEM_ENABLE
        download_region_to_progressor(input_image, region_idx_x, region_idx_y);
#ifdef INTERNAL_COLUMNS_ENABLED
        shift_input_parts_in();
        download_region_to_progressor(input_image, region_idx_x + ACTIVE_PGR_SIDES * pgr_num_rows, region_idx_y);
#endif // INTERNAL_COLUMNS_ENABLED
#endif // READ_GLOBAL_MEM_ENABLE

#ifdef PROCESS_DATA_ENABLE
        //process_part_mb_emulation();
        process_part();
#endif // PROCESS_DATA_ENABLE

#ifdef WRITE_GLOBAL_MEM_ENABLE
#ifdef INTERNAL_COLUMNS_ENABLED
        upload_region_to_gloal_memory(output_image, region_idx_x + ACTIVE_PGR_SIDES * pgr_num_rows, region_idx_y);
        shift_processed_parts_out();
#endif // INTERNAL_COLUMNS_ENABLED
        upload_region_to_gloal_memory(output_image, region_idx_x, region_idx_y);
#endif // WRITE_GLOBAL_MEM_ENABLE
    }


    //printf("--- After data processing ---\n");
    //pgr_mem_ext_a_print_edge_columns(10 * 30, 10, MEM_OUTPUT_MODE_WORD_8_BIT);
    //pgr_mem_ext_a_print_memory(6 * 18, 6, MEM_OUTPUT_MODE_WORD_8_BIT);
}


////////  Master functions  ///////////

int process_image(int num_slaves)
{
#ifdef MOVE_GLOBAL_BUFFERS_TO_HEAP
    // This works only with export MALT_LIBC=newlib in Makefile
    uint8_t * input_image = (uint8_t *)memalign(16, WIDTH * HEIGHT * sizeof(uint8_t) + MEM_ALIGNMENT * 2);
    uint8_t * output_image = (uint8_t *)memalign(16, (WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t) + MEM_ALIGNMENT * 2);

    //uint8_t *input_image = (uint8_t *)malloc(WIDTH * HEIGHT * sizeof(uint8_t) + MEM_ALIGNMENT * 2);
    //uint8_t *output_image = (uint8_t *)malloc((WIDTH - 2) * (HEIGHT - 2) * sizeof(uint8_t) + MEM_ALIGNMENT * 2);

    memcpy(input_image, input_image_temp, WIDTH * HEIGHT * sizeof(uint8_t));
#else
    uint8_t *const input_image = input_image_temp;
    uint8_t *const output_image = output_image_temp;
#endif // MOVE_GLOBAL_BUFFERS_TO_HEAP

    if (((uint32_t)input_image % MEM_ALIGNMENT == 0) && ((uint32_t)output_image % MEM_ALIGNMENT == 0))
    {
        lwlog_info("Alignment OK: %x %x", (uint32_t)input_image, (uint32_t)output_image);
    }
    else
    {
        lwlog_err("Global buffers are not aligned: %x %x", (uint32_t)input_image, (uint32_t)output_image);
    }
    lwlog_debug("pgr_data: %x %x %x", sobel_pgr_kernel, sobel_pgr_data_left, sobel_pgr_data_right);


    lwlog_info("Initialize accelerators...", num_slaves);
    for (int i = 0; i < num_slaves; i++)
    {
        int slave_id = malt_start_thr(device_prepare);
        if (slave_id == -1)
        {
            lwlog_err("Failed to initialize accelerators");
            exit(-1);
        }
    }
    malt_sm_wait_all_lines_free();


    uint64_t t_start_us, t_stop_us;
    uint64_t imem_start_cnt, dmem_start_cnt, total_start_cnt;
    uint64_t imem_stop_cnt, dmem_stop_cnt, total_stop_cnt;

    lwlog_info("Start processing on %d cores...", num_slaves);

    malt_read_pcnt(&imem_start_cnt, &dmem_start_cnt, &total_start_cnt);
    t_start_us = malt_get_time_us();
    malt_start_pcnt();

    // Process on slaves
    for (int i = 0; i < num_slaves; i++)
    {
        int slave_id = malt_start_thr(proces_image_on_core, input_image, output_image, i, num_slaves);
        if (slave_id == -1)
        {
            lwlog_err("Failed to launch code on a slave");
            exit(-1);
        }
    }
    malt_sm_wait_all_lines_free();

    // // Process on master
    // proces_image_on_core(input_image, output_image, 0, 1);
    //malt_start_thr(proces_image_on_core, input_image, output_image, 0, 1);

    //print_output_image(output_image);

    // Processing finished
    malt_stop_pcnt();
    t_stop_us = malt_get_time_us();
    malt_read_pcnt(&imem_stop_cnt, &dmem_stop_cnt, &total_stop_cnt);
    lwlog_info("Processing done!");

    lwlog_info("imem_cnt: %d", imem_stop_cnt - imem_start_cnt);
    lwlog_info("dmem_cnt: %d", dmem_stop_cnt - dmem_start_cnt);
    lwlog_info("total_cnt: %d", total_stop_cnt - total_start_cnt);
    lwlog_info("time_us: %d\n", (uint32_t)(t_stop_us - t_start_us));

#ifdef CHECK_CRC
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, output_image, (WIDTH - 2) * (HEIGHT - 2));
    int crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc, RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }
#endif // CHECK_CRC

#ifdef MOVE_GLOBAL_BUFFERS_TO_HEAP
    free(input_image);
    free(output_image);
#endif // MOVE_GLOBAL_BUFFERS_TO_HEAP

    return 0;
}

int local_stack_main(void)
{
    int num_slaves = malt_get_total_slaves();
    process_image(num_slaves);

    // for (int i = 0; i < num_slaves; i++)
    // {
        // process_image(i + 1);
    // }
    return 0;
}

int main()
{

    return malt_local_stack_call(local_stack_main);
}

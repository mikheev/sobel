#include "malt_ext.h"
#include "stdio.h"
#include "stdbool.h"
#include "string.h"

#include "../../common/sobel_core.h"
#include "../../common/crc_check.h"
#include "../../common/lwlog.h"

#include "../../images/tram_1026.h"


#define NUM_ITERATIONS 3

uint8_t input_image[] = IMAGE_SRC;
uint8_t output_image[(WIDTH - 2) * (HEIGHT - 2)];

int main(void) 
{
    uint64_t t_start_us, t_end_us;
    uint32_t iteration;

    //---------- Обработка изображения ------------
    lwlog_info("Process image %s...", IMAGE_NAME);
    t_start_us = malt_get_time_us();
    for (iteration = 0; iteration < NUM_ITERATIONS; iteration++)
    {
        int x, y;
        for (y = 0; y < (HEIGHT - 2); y++)
        {
            for (x = 0; x < (WIDTH - 2); x++)
            {
                output_image[im_idx((WIDTH - 2), x, y)] = apply_sobel_to_point(input_image, WIDTH, (x + 1), (y + 1));
            }
        }
    }
    t_end_us = malt_get_time_us();
    lwlog_info("Processing done!");
    //---------- Конец обработки изображения ------------

    // Проверка корректности результата
    lwlog_info("Check CRC...");
    uint32_t crc = crc32(0, output_image, (WIDTH - 2) * (HEIGHT - 2));
    bool crc_check_ok = (crc == RESULT_CRC32);

    if (crc_check_ok)
    {
        lwlog_info("CRC is correct. %x", crc, RESULT_CRC32);
    }
    else
    {
        lwlog_err("Wrong CRC!!!. Got %x. Expected %x\n", crc, RESULT_CRC32);
    }

    // Вывод результатов
    char results[256];
    results[0] = '\0';
    sprintf(&results[strlen(results)], "### Sobel results ###\n");
    sprintf(&results[strlen(results)], "Image: %s\n", IMAGE_NAME);
    sprintf(&results[strlen(results)], "Time_ms: %.2f\n", ((float)(t_end_us - t_start_us)) / (NUM_ITERATIONS * 1000));
    sprintf(&results[strlen(results)], "Iterations_done: %d\n", NUM_ITERATIONS);
    sprintf(&results[strlen(results)], "CRC: 0x%x\n", crc);
    sprintf(&results[strlen(results)], "CRC_valid: %s\n", crc_check_ok ? "True" : "False");
    sprintf(&results[strlen(results)], "Comment: \n");
    sprintf(&results[strlen(results)], "\n");
    printf(results);

    return 0;
}
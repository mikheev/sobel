#ifndef __IMAGE_SRC_HEADER
#define __IMAGE_SRC_HEADER

#define WIDTH 6
#define HEIGHT 4
#define IMAGE_SRC {0, 63, 127, 191, 255, 255, 0, 255, 254, 255, 191, 191, 255, 0, 1, 0, 127, 127, 255, 191, 127, 63, 0, 63}
#define IMAGE_NAME "test_a0_5"
#define RESULT_CRC32 0xfb7aa9c3

#endif // __IMAGE_SRC_HEADER